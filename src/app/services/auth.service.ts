import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private regURL = "localhost:3000/api/register";
  private loginURL = "localhost:3000/api/login";

  constructor(private http: HttpClient) { }

  public getLogins(regData) {
    return this.http.post<any>(this.regURL, regData);
  }

  public loginData(loginData) {
    return this.http.post<any>(this.loginURL, loginData);
  }
}

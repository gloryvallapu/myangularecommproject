import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private homeURL = "localhost:3000/api/home";
  constructor(private http:HttpClient) { }

  public getHomepage(){
    return this.http.get<any>(this.homeURL)
  }
}

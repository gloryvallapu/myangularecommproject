import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerUserData = {
    username: '',
    password: ''
  }

  constructor(private authService: AuthService, private router:Router) { }

  ngOnInit(): void {
  }

  public registerUser(){
    this.authService.getLogins(this.registerUserData).subscribe((res)=>{
      console.log(res);
      this.router.navigate(['/login']);
    })
  }
 
}

import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
// import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  // providers: [NgbCarouselConfig]
})
export class HomeComponent implements OnInit {
  public home = [];
  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getHomepage().subscribe((res) => {
      console.log(res)
      this.home = res,
      err => console.log(`error while receivng yhe response from server: ${err}`);
    })
  }

}
